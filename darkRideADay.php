
<?php
 
require 'vendor/autoload.php';
require 'connectionData.php';
 
use Abraham\TwitterOAuth\TwitterOAuth;
 
$connection = new TwitterOAuth(
 CONSUMER_KEY,
 CONSUMER_SECRET,
 ACCESS_TOKEN,
 ACCESS_TOKEN_SECRET
);

$mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
$request = "SELECT darkRide_id, twitt_text, video_link FROM videos WHERE posted = 0 ORDER BY RAND() LIMIT 0, 1";
$darkRide_id = -1;
if ($result = $mysqli->query($request)) {
    while($row = $result->fetch_assoc()) {
        $darkRide_id = $row['darkRide_id'];
        $tweetResult = $connection->post("statuses/update", ["status" => $row["twitt_text"] . " #darkrideaday " . $row["video_link"]]);
    }
    $result->close();
}

if($darkRide_id != -1)
{
    $request = "UPDATE videos SET posted = 1, posted_date=now(), tweet_id = " . $tweetResult->id_str . " WHERE darkRide_id = " . $darkRide_id;    
    $result = $mysqli->query($request);
}

$mysqli->close();
?>
