<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<title>Dark Ride A Day - Summary</title>
		<style type="text/css">
			#chart-container {
				width: 640px;
				height: auto;
			}
      
      twitter-status {
        margin: 8px 0;
      }
        </style>
	</head>
	<body>
    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">About</h4>
              <p class="text-muted">Dark ride a day does just post one video of one dark ride per day on <a href="https://twitter.com/esainton">esainton</a> profile on twitter. This page displays stats about this posts.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <li><a href="https://twitter.com/esainton" class="text-white">Follow on Twitter</a></li>                
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="#" class="navbar-brand d-flex align-items-center">
            <strong>Dark Ride a day</strong>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>
    
    <body>
    <?php
      require 'connectionData.php';
      $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
      $request = "SELECT COUNT(*) remainingVideos FROM videos WHERE tweet_id = 0 OR  tweet_id = ''";      
      $remainingVideosCount = 0;
      if ($result = $mysqli->query($request)) {
          while($row = $result->fetch_assoc()) {
             $remainingVideosCount = $row["remainingVideos"];
          }
          $result->close();
      }
      
      $mysqli->close();
      
    ?>
        <div class="container">
          <br />

          <div class="alert alert-info" role="alert">
            There are still <?php echo $remainingVideosCount; ?> videos ready to be posted.
          </div>
          
            <div class="row">
                <div class="col-md-6">
                    <h1>Dark Ride posted</h1>
                    <div id="lastTweets">
                      
                    </div>
                </div>
            
                <div class="col-md-6" id="chart-container">
                    <h1>Stats</h1>
                    <canvas id="mycanvas"></canvas>
                </div>
            </div>	
        </div>

		<!-- javascript -->
		<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
		<script type="text/javascript" src="node_modules/chart.js/dist/Chart.js"></script>
    <script type="text/javascript" src="js/statsCountries.js"></script>
    <script type="text/javascript" src="js/drPosted.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/twitter-status.min.js"></script>
	</body>
</html>