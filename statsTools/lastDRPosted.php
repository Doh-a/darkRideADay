
<?php
 
 require '../vendor/autoload.php';
 require '../connectionData.php';
  
 use Abraham\TwitterOAuth\TwitterOAuth;
  
 $connection = new TwitterOAuth(
  CONSUMER_KEY,
  CONSUMER_SECRET,
  ACCESS_TOKEN,
  ACCESS_TOKEN_SECRET
 );
 
 $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
 $request = "SELECT tweet_id FROM videos WHERE tweet_id != 0 AND  tweet_id != '' ORDER BY posted_date DESC LIMIT 0, 100";
 $myArray = array();
 if ($result = $mysqli->query($request)) {
     while($row = $result->fetch_assoc()) {
        $statuses = $connection->get("statuses/show", ["id" => $row["tweet_id"], "include_entities" => "true", "tweet_mode" => "extended"]);
        //$statuses->full_text = $statuses->text;        
        //unset($statuses->text);
        $statuses->full_text = str_replace("'", "&apos;", $statuses->full_text);
        $myArray[] = $statuses;
     }
     $result->close();
 }
 
 $mysqli->close();
 
 echo json_encode($myArray);
 ?>
 