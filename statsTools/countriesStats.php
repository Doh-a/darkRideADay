<?php
require '../connectionData.php';

$mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
$request = "SELECT country_name, COUNT(dark_ride_id) totalDR FROM `amusement_parks` p, countries c, dark_rides d WHERE d.park_id = p.park_id AND p.country = c.id GROUP BY c.id";
$result = $mysqli->query($request);
$data = array();
foreach ($result as $row) {
	$data[] = $row;
}

//free memory associated with result
$result->close();

//close connection
$mysqli->close();

//now print the data
print json_encode($data);
?>