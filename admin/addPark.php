<?php
    
    $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
    $request = "SELECT COUNT(*) remainingVideos FROM videos WHERE tweet_id = 0 OR  tweet_id = ''";      
    $remainingVideosCount = 0;
    if ($result = $mysqli->query($request)) {
        while($row = $result->fetch_assoc()) {
            $remainingVideosCount = $row["remainingVideos"];
        }
        $result->close();
    }
    
    $mysqli->close();
    
?>
        
        
<h2>Add another park</h2>

<form action="index.php" method="post">
    <div class="form-group">
        <label for="parkNameInput">Park name</label>
        <input type="text" class="form-control" id="parkNameInput" name="parkNameInput" placeholder="Efteling">
    </div>
    <div class="form-group">
        <label for="countrySelect">Country</label>
        <select class="form-control" id="countrySelect" name="countrySelect">
            <option value="-1">Select a country</option>
            <?php
                $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
                $request = "SELECT id, country_name FROM `countries` ORDER BY country_name";     
                if ($result = $mysqli->query($request)) {
                    while($row = $result->fetch_assoc()) {
                    echo '<option value="' . $row["id"] . '">' . $row["country_name"] . '</option>';
                    }
                    $result->close();
                }
                
                $mysqli->close();
            ?>
        </select>
    </div>   
    <div class="form-group">  
        <input type="hidden" value="action_addPark" name="actionName" id="actionName">                            
        <input type="submit" value="Add" id="submitButton">                                        
    </div>
</form>  