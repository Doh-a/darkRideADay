<h2>Dark Rides listed</h2>
<div class="row">    
    
    <div class="col-4 header">Amusement Park</div>
    <div class="col-4 header">Dark Ride</div>
    <div class="col-4 header">Text</div>

    <?php
        $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
        $request = "SELECT d.dark_ride_name, d.dark_ride_description, p.park_name FROM dark_rides d, amusement_parks p WHERE d.park_id = p.park_id ORDER BY park_name, dark_ride_name";        
        $result = $mysqli->query($request);
        $data = array();
        foreach ($result as $row) {
            echo '<div class="col-4">' . $row["park_name"] . '</div><div class="col-4">' . $row["dark_ride_name"] . '</div><div class="col-4">' . $row["dark_ride_description"] . '</div>';
        }

        //free memory associated with result
        $result->close();

        //close connection
        $mysqli->close();
    ?>
</div>