<?php
    
    $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
    $request = "SELECT COUNT(*) remainingVideos FROM videos WHERE tweet_id = 0 OR  tweet_id = ''";      
    $remainingVideosCount = 0;
    if ($result = $mysqli->query($request)) {
        while($row = $result->fetch_assoc()) {
            $remainingVideosCount = $row["remainingVideos"];
        }
        $result->close();
    }
    
    $mysqli->close();
    
?>
        
        
<h2>Add another dark ride</h2>

<form action="index.php" method="post">
    <div class="form-group">
        <label for="darkRideNameInput">Dark ride name</label>
        <input type="text" class="form-control" id="darkRideNameInput" name="darkRideNameInput" placeholder="Mystic Manor">
    </div>
    <div class="form-group">
        <label for="tweetTextArea">Tweet</label>
        <textarea class="form-control" id="tweetTextArea" name="tweetTextArea" rows="3"></textarea>
        <input type="text" id="count-characters-result" readonly> characters.
    </div>
    <div class="form-group">
        <label for="videoLinkTextInput">Video link</label>
        <input type="text" class="form-control" id="videoLinkTextInput" name="videoLinkTextInput" placeholder="https://www.youtube.com/watch?v=k5l_Q7Ws7Ks">
    </div>
    <div class="form-group">
        <label for="amusementParkSelect">Amusement park</label>
        <select class="form-control" id="amusementParkSelect" name="amusementParkSelect">
            <option value="-1">Select a park</option>
            <?php
                $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
                $request = "SELECT park_id, park_name FROM `amusement_parks` ORDER BY park_name";      
                $remainingVideosCount = 0;
                if ($result = $mysqli->query($request)) {
                    while($row = $result->fetch_assoc()) {
                    echo '<option value="' . $row["park_id"] . '">' . $row["park_name"] . '</option>';
                    }
                    $result->close();
                }
                
                $mysqli->close();
            ?>
        </select>
    </div>   
    <div class="form-group">    
        <input type="hidden" value="action_addDR" id="actionName">                          
        <input type="submit" value="Add" id="submitButton">                                        
    </div>
</form>                   
		
<!-- javascript -->
<script src="../node_modules/javascript-character-count/dist/javascript-character-count.min.js"></script>
<script>
    // Pass input element as first argument. 
    // with jquery: $('#count-characters'); 
    // Options array as second argument. 
    characterCount(document.getElementById('tweetTextArea'), {
        max: 222, // the maximum input length (use for ux or callback) 
        // Pass output element to the output key. 
        output: document.getElementById('count-characters-result')
    },
    // Passing the callback function as third parameter. 
    function () {
        alert('You entered the maximum number of characters');
    }, true);
</script>