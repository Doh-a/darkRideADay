<?php
        $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
        $request = "SELECT dark_ride_id, dark_ride_name, park_name FROM dark_rides, amusement_parks WHERE dark_rides.park_id = amusement_parks.park_id AND amusement_parks.park_id = " . $_GET['park'];        
        $result = $mysqli->query($request);
?>
    
    <?php        
        $first = true;
        foreach ($result as $row) {            
            if($first)
            {
                echo '<h2>Dark Ride at ' . $row['park_name'] . '</h2> <ul>';
                $first = false;
            }

            echo '<li><a href="index.php?page=detailsDR=' . $row["dark_ride_id"] . '">' . $row["dark_ride_name"] . '</a></li>';
        }

        //free memory associated with result
        $result->close();

        //close connection
        $mysqli->close();
    ?>
</ul>