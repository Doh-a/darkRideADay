<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link <?php if($page_action == 'stats') echo 'active'; ?>" href="index.php">
                <span data-feather="home"></span>
                Dashboard
                </a>
            </li>                                                
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Dark rides</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
        </h6>
        <ul class="nav flex-column mb-2">
        <li class="nav-item">
                <a class="nav-link <?php if($page_action == 'listDR') echo 'active'; ?>" href="index.php?page=listDR">
                <span data-feather="file"></span>
                List Dark Rides
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link <?php if($page_action == 'addDR') echo 'active'; ?>" href="index.php?page=addDR">
                <span data-feather="file"></span>
                Add Dark Ride
                </a>
            </li>            
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Parks</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link <?php if($page_action == 'listParks') echo 'active'; ?>" href="index.php?page=listParks">
                <span data-feather="file"></span>
                List Parks
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php if($page_action == 'addPark') echo 'active'; ?>" href="index.php?page=addPark">
                <span data-feather="shopping-cart"></span>
                Add Park
                </a>
            </li>
        </ul>
    </div>
</nav>
