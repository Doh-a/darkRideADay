<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<title>Dark Ride A Day - Summary</title>
		<style type="text/css">
			#chart-container {
				width: 640px;
				height: auto;
			}
      
            twitter-status {
                margin: 8px 0;
            }
        </style>

        <!-- Custom styles for this template -->
        <link href="../css/dashboard.css" rel="stylesheet">
	</head>

	<body>
        <?php 
            require '../connectionData.php';
            include 'header.php'; 
        ?>

        <div class="container-fluid">    
            <div class="row">
                <?php
                    $page_action = 'login';

                    // Does the user log or unlog?
                    if(isset($_GET['page']) && $_GET['page'] == 'logout')
                    {
                        session_destroy();
                        $_GET['page'] = 'login';
                    }

                    if(isset($_POST['actionName']) && ($_POST['actionName'] == 'login'))
                    {
                        $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
                        $request = "SELECT u.username, u.user_id FROM users u WHERE u.username = '" . $_POST["inputPseudo"] . "' AND u.password = '" . md5($_POST["inputPassword"]) ."'";        
                        
                        $result = $mysqli->query($request);
                        $data = array();
                        if($result->num_rows == 1)
                        {
                            foreach($result as $row)
                            {
                                $_SESSION['lastActiveDate'] = time();
                                $_SESSION['userName'] = $row["username"];
                                $_SESSION['userId'] = $row["user_id"];
                            }

                            $page_action = 'stats';
                        }                        

                        //free memory associated with result
                        $result->close();

                        //close connection
                        $mysqli->close();
                    }

                    if(isset($_SESSION['lastActiveDate']) && (time()-$_SESSION['lastActiveDate']) < 3600) 
                    {
                        $_SESSION['lastActiveDate'] = time();

                        if(isset($_POST['actionName']) && ($_POST['actionName'] == 'action_addDR'))
                        {
                            include "action_addDR.php";
                        }
                        
                        if(isset($_POST['actionName']) && ($_POST['actionName'] == 'action_addPark'))
                        {
                            include "action_addPark.php";
                            $_GET['page'] = 'listParks';
                        }

                        if(isset($_GET['page']))
                        {
                            switch($_GET['page'])
                            {
                                case 'addDR' :
                                    $page_action = 'addDR';
                                    break;
                                case 'listDR' :
                                    $page_action = 'stats';
                                    break;
                                case 'addPark' :
                                    $page_action = 'addPark';
                                    break;
                                case 'listParks' :
                                    $page_action = 'listParks';
                                    break;
                                case 'logout' :
                                    $page_action = 'logout';
                                    break;
                                case 'showDR' : 
                                    $page_action = 'showDR';
                                    break;
                                default:
                                    $page_action = 'stats';
                            }
                        }
                        else
                            $page_action = 'stats';
                    }
                    else
                    {
                        $page_action = 'login';                        
                    }
                
                    include 'menu.php' 
                ?>
    
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                    <div class="fluid justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">

                        <?php
                            switch($page_action)
                            {
                                case 'login':
                                    include 'login.php';
                                    break;
                                case 'logout';
                                    include 'logout.php';
                                    break;
                                case 'stats':                                    
                                    include 'statsContent.php'; 
                                    break;
                                case 'addDR':
                                    include 'addDR.php';
                                    break;
                                case 'addPark':
                                    include 'addPark.php';
                                    break;
                                case 'listParks':
                                    include 'listParks.php';
                                    break;
                                case 'showDR':
                                    include 'listDRInParks.php';
                                    break;
                                default :
                                    include 'statsContent.php';
                                    break;
                            }
                        ?>

                    </div>
                </main>                        
            </div>
        </div>
            
        <!-- javascript -->
        <script type="text/javascript" src="../node_modules/jquery/dist/jquery.min.js"></script>
        <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>        
	</body>
</html>