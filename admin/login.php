<h2 class="h3 mb-3 font-weight-normal">Please sign in</h2>
<form action="index.php" method="post">
    <label for="inputPseudo" class="sr-only">Pseudo</label>
    <input type="text" id="inputPseudo" name="inputPseudo" class="form-control" placeholder="Pseudo" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
    <input type="hidden" name="actionName" value="login" />
    <br />
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>