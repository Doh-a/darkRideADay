<h2>Parks in the database</h2>

    <?php
        $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_BASE) or die('Error connecting to MySQL server.');
        $request = "SELECT c.continent_name, ap.park_name, ap.park_id, COUNT(d.dark_ride_id) AS drCount, c.country_name FROM `amusement_parks` ap LEFT JOIN `countries` c ON ap.country = c.id LEFT JOIN `dark_rides` d ON ap.park_id = d.park_id GROUP BY(ap.park_id) ORDER BY c.continent_name, ap.park_name ";        
        $result = $mysqli->query($request);
        $data = array();
        $lastContinentName = "";
        foreach ($result as $row) {
            if($row["continent_name"] != $lastContinentName)
            {
                if($lastContinentName != "")
                    echo '</div><br />';

                $lastContinentName = $row["continent_name"];

                echo '<h3>' . $lastContinentName . '</h3>';

                echo '<div class="row"><div class="col-4 header">Amusement Park</div><div class="col-4 header">Dark Ride Count</div><div class="col-4 header">Country</div>';
            }
            echo '<div class="col-4"><a href="index.php?page=showDR&park=' . $row["park_id"] . '">' . $row["park_name"] . '</a></div><div class="col-4">' . $row["drCount"] . '</div><div class="col-4">' . $row["country_name"] . '</div>';
        }

        //free memory associated with result
        $result->close();

        //close connection
        $mysqli->close();
    ?>
</div>