$(document).ready(function(){
	$.ajax({
		url: "statsTools/countriesStats.php",
        method: "GET",
        dataType: 'json',
		success: function(data) {
			var country = [];
            var drCount = [];
            var color = [];

			for(var i in data) {
                country.push(data[i].country_name);
                drCount.push(data[i].totalDR);
                color.push(getRandomColor());
			}
            
            var chartdata = {
				labels: country,
				datasets : [
					{
                        label: 'Countries dark rides',
                        backgroundColor : color,
						data: drCount
					}
				],
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'pie',
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}